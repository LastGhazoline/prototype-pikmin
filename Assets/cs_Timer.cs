﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class cs_Timer : MonoBehaviour
{
    private Text text;

    float timeRemaining;

    // Start is called before the first frame update
    void Start()
    {
        timeRemaining = 5;
        text = gameObject.GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.GameEnd)
        {
            timeRemaining = 5;
            text.text = "";
        }

        if (GameManager.GameEnd)
        {
            float seconds = Mathf.FloorToInt(timeRemaining % 60);

            text.text = "Restart in " + seconds;

            if (timeRemaining>1)
            {
                timeRemaining -= Time.deltaTime;
            }


        }

    }
}
