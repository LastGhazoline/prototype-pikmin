﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraPointer : MonoBehaviour
{

   public GameObject LookAt;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if ((collision.CompareTag("EnemyControllable") || collision.CompareTag("Player")) && !collision.isTrigger)
        {
            LookAt.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if ((collision.CompareTag("EnemyControllable") || collision.CompareTag("Player")) && !collision.isTrigger)
        {
            LookAt.SetActive(false);
        }
    }


}
