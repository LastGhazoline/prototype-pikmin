﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyControlling : MonoBehaviour
{
    public enum en_Control { outOfControl, inControl }
    public en_Control test;


    public CharacterController2D controller;

    public float runSpeed = 10f;

    float horizontalMove = 0f;
    bool jump = false;
    bool crouch = false;

    public GameObject theirCasquette;

    public GameObject player;
    // Update is called once per frame
    void Update()
    {
        switch (test)
        {
            case en_Control.inControl:
                theirCasquette.SetActive(true);
                horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;
                Debug.Log(crouch);
                if (Input.GetButtonDown("Jump"))
                {
                    jump = true;
                }

                if (Input.GetButtonDown("Fire2"))
                {
                    theirCasquette.SetActive(false);
                    test = en_Control.outOfControl;
                    Instantiate(player, transform.position, new Quaternion(0,0,0,0));
                }

                break;
        }
    }

    void FixedUpdate()
    {
        switch (test)
        {
            case en_Control.inControl:
                controller.Move(horizontalMove * Time.fixedDeltaTime, crouch, jump);
                jump = false;
                break;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Casquette"))
        {
            test = en_Control.inControl;
            PlayerMovement.controllingStart = true;
        }

    }

}
