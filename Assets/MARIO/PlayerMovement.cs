﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController2D controller;

    public float runSpeed = 10f;

    float horizontalMove = 0f;
    bool jump = false;
    bool crouch = false;


    public GameObject casquette;
    public Transform casquetteSpawn;

    static public bool controllingStart = false;
    // Start is called before the first frame update


    private void Start()
    {
        controllingStart = false;
        
    }
    void Update()
    {
        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;

        if (Input.GetButtonDown("Jump"))
        {
            jump = true;
        }

        if (Input.GetButtonDown("Crouch"))
        {
            crouch = true;
        }else if (Input.GetButtonUp("Crouch"))
        {
            crouch = false;
        }

        if(Input.GetButtonDown("Fire1"))
        {
            Instantiate(casquette, casquetteSpawn.position, casquetteSpawn.rotation);
           
        }

        if (controllingStart)
        {
            gameObject.SetActive(false);
        }

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        controller.Move(horizontalMove * Time.fixedDeltaTime, crouch, jump);
        jump = false;
    }
}
