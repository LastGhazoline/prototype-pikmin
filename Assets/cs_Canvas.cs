﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class cs_Canvas : MonoBehaviour
{

    public KeyCode name;
    // Start is called before the first frame update
    void Start()
    {
      
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(name))
        {
            gameObject.GetComponent<Image>().color = Color.red;
        }
        if (Input.GetKeyUp(name))
        {
            gameObject.GetComponent<Image>().color = Color.white;
        }
    }
}
