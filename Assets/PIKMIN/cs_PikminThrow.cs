﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cs_PikminThrow : MonoBehaviour
{
    public float speed = 10f;
    public float knockDuration = 1f;
    public float strenght = 50f;
    public Rigidbody2D rb;

    public GameObject burst;
    // Start is called before the first frame update
    void Start()
    {
        rb.velocity = transform.right * speed;
        Destroy(gameObject, 1f);
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
            
        if (collision.gameObject.CompareTag("Player") || collision.gameObject.CompareTag("PlayerWithPik"))
        {
            ScreenShakeController.instance.StartShake(0.2f, 0.2f);
            GameObject Burst = Instantiate(burst, transform.position, transform.rotation) as GameObject;
            Burst.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
            Destroy(gameObject);
            //StartCoroutine(cs_PlayerControl2.instance.Knock(knockDuration, strenght, this.transform));
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
               if (!collision.gameObject.CompareTag("Lava"))
        {
            speed = speed + 100;
             ScreenShakeController.instance.StartShake(0.1f, 0.05f);
        }
    }
}
