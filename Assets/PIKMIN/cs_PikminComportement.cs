﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cs_PikminComportement : MonoBehaviour
{

    public float size;

    private float beforeBloom;
    public float beforeBloomLimit;
    public float bloomingTime;
    public float extractionTime;

    public float pikminThrowSpeed;
    public float pikminThrowStrenght;
    public Color pikminColor;


    private float nextBloom;

    private bool onExtraction = false;
    public int chaussette;

    // Start is called before the first frame update
    void Start()
    {
            pikminThrowSpeed = 5;
            pikminThrowStrenght = 0.005f;
            size = 0.2f;
            pikminColor = new Color(0, 0, 0.99f);
            gameObject.transform.localScale = new Vector2(size, size);
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > nextBloom && beforeBloom < bloomingTime && !onExtraction)
        {
            nextBloom = Time.time + beforeBloomLimit;
            beforeBloom += 1;
        }
        else if (beforeBloom >= bloomingTime)
        {
            Destroy(gameObject);
            cs_SpanwPikmin.pikminSpawned -= 1;
        }
    }

    void OnTriggerStay2D(Collider2D collision)
    {
        
        if (collision.gameObject.CompareTag("Player"))
        {
           // Debug.Log("yeah man");
                onExtraction = true;
             //0.05   Debug.Log("no man");
                cs_SpanwPikmin.pikminSpawned -= 1;
                collision.gameObject.tag = "PlayerWithPik";
                
                Destroy(gameObject);
                
            }
        
    }
}
