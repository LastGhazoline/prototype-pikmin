﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cs_lava : MonoBehaviour
{
    public GameObject burst;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.CompareTag("Player") || collision.gameObject.CompareTag("PlayerWithPik"))
        {
            ScreenShakeController.instance.StartShake(0.6f, 0.6f);
            if (collision.name == "Player1")
            {
                GameManager.pointP2 += 1;
                GameManager.whoWinThis = 2;
            }else
            {
                GameManager.pointP1 += 1;
                GameManager.whoWinThis = 1;
            }
            GameManager.GameEnd = true;

            GameObject Burst = Instantiate(burst, collision.transform.position, collision.transform.rotation) as GameObject;
            Burst.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
            Destroy(collision.gameObject);
        }

    }
}
