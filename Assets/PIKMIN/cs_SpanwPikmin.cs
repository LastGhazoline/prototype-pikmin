﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cs_SpanwPikmin : MonoBehaviour
{
    public GameObject pikminSpawn;

    public Transform[] limitSpawn;

    public float spawnRate = 0.2f;
    private float nextSpawn;

    public static int pikminSpawned = 0;
    public int pikminMax = 40;

    private void Start()
    {
        pikminSpawned = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Time.time > nextSpawn && pikminSpawned<pikminMax)
        {
            float x = Random.Range(limitSpawn[0].position.x, limitSpawn[1].position.x);
            float y = Random.Range(limitSpawn[0].position.y, limitSpawn[1].position.y);
            Instantiate(pikminSpawn, new Vector2(x, y), pikminSpawn.transform.rotation);
            pikminSpawned += 1;
            nextSpawn = Time.time + spawnRate;
        }

    }
}
