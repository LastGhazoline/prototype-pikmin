﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class cs_PlayerControl2 : MonoBehaviour
{
    static public bool onHand;
    public SpriteRenderer pikminInHand;
    public static cs_PlayerControl2 instance;
    public float runSpeed = 3f;
    public float boost = 1f;
    public bool actionBoost = true;
    public bool canShoot = true;
    float horizontalMove = 0f;
    float verticalMove = 0f;
    public string[] mapping; 

    public GameObject shotPrefab;
    public Transform shotPoint;
    public Transform rotatePoint;

    public Text text;

    private Rigidbody2D rb;


    private float knockbackstop = 0;
    private float knockback = 1;

    public Animator animator;

    private void Awake()
    {
        instance = this; 
    }

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }


    // Update is called once per frame
    void Update()
    {
        
       // if (Input.GetButtonDown("Fire2") && actionBoost == true)
        //{
       //     StartCoroutine(SpeedUp());
       // }

        if (!GameManager.GameEnd)
        { 
        //Debug.Log(gameObject.tag);
        horizontalMove = Input.GetAxisRaw(mapping[0]) * runSpeed * boost;
        verticalMove = Input.GetAxisRaw(mapping[1]) * runSpeed * boost;

        if (horizontalMove > 0 || verticalMove > 0)
        {
            animator.SetBool("onWalk", true);
        }
        else
        {
            animator.SetBool("onWalk", false);
        }

        if (horizontalMove < 0)
        {
            gameObject.GetComponent<SpriteRenderer>().flipX = true;
        } else
            gameObject.GetComponent<SpriteRenderer>().flipX = false;

        float angle = Mathf.Atan2(verticalMove, horizontalMove) * Mathf.Rad2Deg;
        rotatePoint.rotation = Quaternion.Euler(new Vector3(0, 0, angle));

        if(gameObject.CompareTag("PlayerWithPik"))
        {
            pikminInHand.gameObject.SetActive(true);
        }
        else { pikminInHand.gameObject.SetActive(false); }


        if (gameObject.CompareTag("PlayerWithPik") && Input.GetButtonDown(mapping[2]) && canShoot == true)
        {
            Instantiate(shotPrefab, shotPoint.position, shotPoint.rotation);
            gameObject.tag = "Player";
        }
        }

        if (GameManager.GameEnd)
        {
            runSpeed = 0;
        }
    }

    void FixedUpdate()
    {
        if (!GameManager.GameEnd)
        transform.Translate(new Vector2(horizontalMove, verticalMove) * Time.fixedDeltaTime);
    }

    private void OnTriggerStay2D (Collider2D other)
    {
        if (gameObject.CompareTag("Player"))
        { 
        cs_PikminThrow throught = shotPrefab.GetComponent<cs_PikminThrow>();
        cs_PikminComportement info = other.GetComponent<cs_PikminComportement>();
        throught.speed = info.pikminThrowSpeed;
        throught.strenght = info.pikminThrowStrenght;
            pikminInHand.color = info.pikminColor;

            SpriteRenderer colorShot = shotPrefab.GetComponent<SpriteRenderer>();
            colorShot.color = info.pikminColor;
        }
    }

   // public IEnumerator Knock(float knockbackDuration, float knockbackPower, Transform obj)
    //{
      //  float timer = 0;
        // while (knockbackDuration > timer)
        //{
         //   timer = Time.deltaTime;
         //   Vector2 direction = (obj.transform.position - this.transform.position).normalized;
         //   rb.AddForce(-direction * knockbackPower);
       // }

//        yield return 0;
  //  }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!GameManager.GameEnd)
        { 
        if (collision.CompareTag("Bullet"))
        {
            cs_PikminThrow power = collision.GetComponent<cs_PikminThrow>();

            rb.isKinematic = false;
            Vector2 direction = (transform.position - collision.transform.position);
            direction = direction.normalized * (power.strenght);
            rb.AddForce(direction, ForceMode2D.Impulse);
            StartCoroutine(KnockCo());
        }
        }
    }


    private IEnumerator KnockCo()
    {
        animator.SetBool("hurt", true);
        yield return new WaitForSeconds(0.1f);
        
        rb.velocity = Vector2.zero;
        rb.isKinematic = true;

        yield return new WaitForSeconds(0.4f);
        animator.SetBool("hurt", false);
    }

    IEnumerator SpeedUp()
    {
        actionBoost = false;
        canShoot = false;
        boost = 2f;
        yield return new WaitForSeconds(2);
        canShoot = true;
        boost = 1f;
        yield return new WaitForSeconds(4);
        actionBoost = true;
    }
}
