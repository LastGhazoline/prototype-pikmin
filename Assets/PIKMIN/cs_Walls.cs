﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cs_Walls : MonoBehaviour
{

    public List<Sprite> image;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {

        StartCoroutine(BigUp());
    }


    IEnumerator BigUp()
    {
        gameObject.GetComponent<SpriteRenderer>().sprite = image[1];
        gameObject.transform.localScale = new Vector3(0.25f, 0.25f, 0.25f);
        yield return new WaitForSeconds(0.1f);
        gameObject.GetComponent<SpriteRenderer>().sprite = image[0];
        gameObject.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f
            );
    }
}
