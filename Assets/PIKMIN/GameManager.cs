﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public Text text;

    static public float pointP1, pointP2;

    public Text tPointP1;
    public Text tPointP2;

    static public int whoWinThis;
    public Text whoWin;

    public GameObject restart;

    public static bool GameEnd = false;


    public Text Rounding;
    public static int Round = 1;

    public Text Timer;
    public static float timing;
    // Start is called before the first frame update
    void Start()
    {
        restart.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("PTN DE " + (Input.GetButton("Fire1") && Input.GetButton("Fire2")));
        tPointP1.text = "" + pointP1;
        tPointP2.text = "" + pointP2;
        text.text = "";
        whoWin.text = "";
        Rounding.text = "Round n°" + Round;

     //   timing = Time.deltaTime;

     //   float voix = Mathf.FloorToInt(timing % 60);

        //Timer.text = voix + " s" ;

       // Debug.Log(timing);

        if (GameEnd)
        {
            
            text.text = "END";
            whoWin.text = "PLAYER " + whoWinThis + " WIN";
            restart.SetActive(true);

            if (Input.GetButton("Fire1") && Input.GetButton("Fire2"))
            {
                Round += 1;
                GameEnd = false;
                SceneManager.LoadScene("PIKMINSOLO");
            }
            
        }
    }

    private void Restart()
    {
        GameEnd = false;
        SceneManager.LoadScene("PIKMINSOLO");
        
        
    }
}
